## Kom igång
Kör dessa kommandon i terminalen för att sätta upp och köra igång appen.
```sh
$ npm install
$ npm start
```
Det första kommandot laddar ned ett gäng saker som står listade i package.json.

Gör vad ni vill med koden :)