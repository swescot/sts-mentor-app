import React from 'react';
import './App.scss';
import List from './components/list/list';
import Card from './components/card/card';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemon: [],
      selectedPokemon: undefined
    };
  }

  componentDidMount() {
    fetch("https://pokeapi.co/api/v2/pokemon?limit=151")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            pokemon: result.results
          });
          this.selectPokemon(65);
        },
        (error) => {
          this.setState({
          });
        }
      )
  }

  selectPokemon(index) {
    this.setState({
      selectedPokemon: undefined
    })
    fetch(this.state.pokemon[index].url)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            selectedPokemon: result
          })
        },
        (error) => {
          this.setState({
          });
        }
      )

  }

  render() {
    return (
      <div className="App">
        <List pokemon={this.state.pokemon} selectPokemon={this.selectPokemon.bind(this)}/>
        <Card pokemon={this.state.selectedPokemon} />
      </div>
    );
  }
}

export default App;
