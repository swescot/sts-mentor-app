import React from 'react';
import './card.scss';

class Card extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
    };
	}


  render() {

		const title = this.props.pokemon ? this.props.pokemon.name : 'loading...'
		const imgSrc = this.props.pokemon ? this.props.pokemon.sprites.front_default : ''

		return (
			<div className='card'>
				<h2>
					{title}
				</h2>
				<div className='img-container'>
					<img src={imgSrc} />
				</div>
			</div>
			);
  }
}

export default Card;