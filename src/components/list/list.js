import React from 'react';
import ListItem from './list-item';
import './list.scss';

class List extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
    };
	}

  render() {
		const listItems = this.props.pokemon.map((pokemon, index) =>
			<ListItem name={pokemon.name} url={pokemon.url} key={index} index={index} selectPokemon={this.props.selectPokemon}/>
    );

		return (
			<div className="pokemon-list">
				<h2>Pokemon</h2>
				<ul>{listItems}</ul>
			</div>
			);
  }
}

export default List;