import React from 'react';

class ListItem extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
    };
	}

	componentDidMount() {
  }

	render() {
		return (
			<li><button onClick={() => this.props.selectPokemon(this.props.index)}>{this.props.name}</button></li>
		)
	}
}

export default ListItem;